import React, { Component } from 'react'
import axios from 'axios'
import ItemLista from './ItemLista'


export default class Home extends Component {

    state = {
        posts: [],
        comments: []
    }

    componentDidMount() {
        

        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(res => {
                const posts = res.data
                this.setState({ posts })
            })
        
        axios.get('https://jsonplaceholder.typicode.com/comments')
            .then(res => {
                const comments = res.data
                this.setState({ comments })
            })
    }


   
    render() {
        return (
            <div className="container mt-2">
                <h1>Listagem de Produtos</h1>
                <div className="row">
                {this.state.posts.map((post) => (
                            <ItemLista
                                key={post.id}
                                title={post.title}
                                body={post.body}
                               
                            />
                        ))}
 
     </div>
            </div>
        )
    }
}

