import React from 'react';

const ItemLista = props => (
    
    <div className="card col-sm-3 col-1 mt-2">
     <div className="card-body ">
     <h2 className="card-title border border-primary">{props.title}</h2>
     <h5 className="card-text">{props.body}</h5>
     <h3 className="card-title">Comentários</h3>
     <h4 className="card-text">{props.name}</h4>
     </div>
    </div>

)

export default ItemLista
